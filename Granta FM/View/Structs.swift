//
//  Structs.swift
//  Granta FM
//
//  Created by Юрий Ершов on 31.07.2019.
//  Copyright © 2019 ITELMA. All rights reserved.
//

import Foundation

public struct soundEQ {
    var NC: Int
    var SC: Int
    var VC: Int
    
    init(NC: Int, SC: Int, VC: Int) {
        self.NC = NC
        self.SC = SC
        self.VC = VC
        
    }
}

public struct ServiseCarriageReturn {
    var CarriageReturnEnding:String = "[0x0D]"
}


public enum ServiceNameSet: String {
    case Volume = "\"vol=\""
    case Frequency = "\"freq=\""
    case LowbandLevel = "\"low=\""
    case MidbandLevel = "\"mid=\""
    case HighbandLevel = "\"high=\""
    case Search = "\"search=\""
    case SoundSource = "\"sound=\""
    case OutsideLocker = "\"outside=\""
    case MuteSound = "\"mute=\""
    case TyreSensorNtraining = "\"ttrain"
    case VehicleLatitude = "\"lat=\""
    case VehicleLongitude = "\"lon=\""
    case VehicleLocationTimestamp = "\"lts=\""
    case IgnitionState = "\"ign=\""
    
    case TyrePressureSensorN1 = "\"tpres\"{1}\"=\""
    case TyreTemperatureSensorN1 = "\"ttemp\"{1}\"=\""
    case TyreSensorsNVoltage1 = "\"tvolt\"{1}\"=\""
    case TyreNLeakage1 = "\"tleak\"{1}\"=\""
    case TyreSensorNLowVoltage1 = "\"tlowv\"{1}\"=\""
    case TyreSensorNnoConnection1 =  "tnoconn\"{1}\"=\""
    case TyreSensorNlearning1 = "\"tlearn\"{1}\"=\""
    
    case TyrePressureSensorN2 = "\"tpres\"{2}\"=\""
    case TyreTemperatureSensorN2 = "\"ttemp\"{2}\"=\""
    case TyreSensorsNVoltage2 = "\"tvolt\"{2}\"=\""
    case TyreNLeakage2 = "\"tleak\"{2}\"=\""
    case TyreSensorNLowVoltage2 = "\"tlowv\"{2}\"=\""
    case TyreSensorNnoConnection2 =  "tnoconn\"{2}\"=\""
    case TyreSensorNlearning2 = "\"tlearn\"{2}\"=\""
    
    case TyrePressureSensorN3 = "\"tpres\"{3}\"=\""
    case TyreTemperatureSensorN3 = "\"ttemp\"{3}\"=\""
    case TyreSensorsNVoltage3 = "\"tvolt\"{3}\"=\""
    case TyreNLeakage3 = "\"tleak\"{3}\"=\""
    case TyreSensorNLowVoltage3 = "\"tlowv\"{3}\"=\""
    case TyreSensorNnoConnection3 =  "tnoconn\"{3}\"=\""
    case TyreSensorNlearning3 = "\"tlearn\"{3}\"=\""
    
    case TyrePressureSensorN4 = "\"tpres\"{4}\"=\""
    case TyreTemperatureSensorN4 = "\"ttemp\"{4}\"=\""
    case TyreSensorsNVoltage4 = "\"tvolt\"{4}\"=\""
    case TyreNLeakage4 = "\"tleak\"{4}\"=\""
    case TyreSensorNLowVoltage4 = "\"tlowv\"{4}\"=\""
    case TyreSensorNnoConnection4 =  "tnoconn\"{4}\"=\""
    case TyreSensorNlearning4 = "\"tlearn\"{4}\"=\""
}

public func GetDataForServeseSet (NameService: ServiceNameSet, volume: Any) -> (String?) {
    let END: String = ServiseCarriageReturn().CarriageReturnEnding
    
    switch NameService {
    case .Volume:
        guard let V = StringFormat3FromInt0to100(volume: volume) else {print ("Error V"); return nil}
        return "\(NameService.rawValue)\(V)\(END)"
        
    case .Frequency:
        guard let V = StringFormat5FromFloat88to108(volume: volume) else {print ("Error V"); return nil}
        return "\(NameService.rawValue)\(V)\(END)"
        
    case .LowbandLevel:
        guard let V = StringFormat3FromInt0to100(volume: volume) else {print ("Error V"); return nil}
        return "\(NameService.rawValue)\(V)\(END)"
    case .MidbandLevel:
        guard let V = StringFormat3FromInt0to100(volume: volume) else {print ("Error V"); return nil}
        return "\(NameService.rawValue)\(V)\(END)"
    case .HighbandLevel:
        guard let V = StringFormat3FromInt0to100(volume: volume) else {print ("Error V"); return nil}
        return "\(NameService.rawValue)\(V)\(END)"
        
    case .Search:
        guard let V: String = String(volume as! String) else {print ("Error V"); return nil}
        switch V {
        case "up":      return "\"up\""
        case "down":    return "\"down\""
        default:            return nil
        }
        
    default:
        print("Error get Volume")
        return "switch error"
    }
}

public func StringFormat3FromInt0to100 (volume: Any) -> (String?) {
    guard let v: Int = Int(volume as! Double), v < 101, v >= 0 else { print("Error volume \(volume)"); return nil}
    switch v {
    case 100 :
        return "{\"\(v)\"}"
    case 0 :
        return "{\"000\"}"
    case 9...100 :
        return "{\"0\(v)\"}"
    case 0...10 :
        return "{\"00\(v)\"}"
    default:
        return nil
    }
}

public func StringFormat5FromFloat88to108 (volume: Any) -> (String?) {
    guard let v: Double = Double(volume as! Double), v < 108.1, v >= 87.9 else { print("Error volume \(volume)"); return nil}
    switch v {
    case 99.9...108.0 :
        let V = String(format: "%.1f", v)
        return "{\"\(V)\"}"
    case 88.0...100.0 :
        let V = String(format: "%.1f", v)
        return "{\"0\(V)\"}"
    default:
        return nil
    }
}
