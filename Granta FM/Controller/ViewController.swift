//
//  ViewController.swift
//  Granta FM
//
//  Created by Юрий Ершов on 29.07.2019.
//  Copyright © 2019 ITELMA. All rights reserved.
//

import UIKit
import AudioToolbox

class ViewController: UIViewController {
    
    

    // добавляем вертикальный слайдер
    @IBOutlet weak var SliderVolume: UISlider! {
        didSet {
            SliderVolume.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
            SliderVolume.setThumbImage(UIImage(named: "SliderVolThumb2"), for: .normal)
            SliderVolume.setThumbImage(UIImage(named: "SliderVolThumb2"), for: .selected)
            SliderVolume.setThumbImage(UIImage(named: "SliderVolThumb2"), for: .highlighted)
        }
    }
    var soundLevelslider = UISlider()
    
    var soundLevel: Float = 50
    var mute: Bool = false
    
    @IBOutlet weak var SliderVolumeWidthConstr: NSLayoutConstraint!
    @IBOutlet weak var BackGroundSoundLevel: UIImageView!
    @IBOutlet weak var SoundLevelBackGroundWigthCoxtr: NSLayoutConstraint!
    @IBOutlet weak var ViewSoundLevel: UIView!
    @IBOutlet weak var SliderFMOutlet: UISlider!
    @IBOutlet weak var SoundLevelIndOutlet: UIButton!
    @IBOutlet weak var RadioButtonOutlet: UIButton!
    @IBOutlet weak var FMlevelOutlet: UILabel!
    @IBOutlet weak var Button1: UIButton!
    @IBOutlet weak var Button2: UIButton!
    @IBOutlet weak var Button3: UIButton!
    @IBOutlet weak var Button4: UIButton!
    @IBOutlet weak var Button5: UIButton!
    @IBOutlet weak var Button6: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        SliderFMOutlet.setThumbImage(UIImage(named: "Sign"), for: .normal)
        SliderFMOutlet.setThumbImage(UIImage(named: "Sign"), for: .selected)
        SliderFMOutlet.setThumbImage(UIImage(named: "Sign"), for: .highlighted)
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
        SliderVolumeWidthConstr.constant = BackGroundSoundLevel.frame.height
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SliderVolumeWidthConstr.constant = BackGroundSoundLevel.frame.height
    }
    
    @IBAction func SoundLevelStateButton(_ sender: UIButton) {
        muteSound()
    }
    @IBAction func RadioButtonAction(_ sender: UIButton) {
        muteSound()
    }
    
    @IBAction func Button1(_ sender: UIButton) {
        SliderFMOutlet.value = Float(sender.titleLabel!.text!)!
        FMlevelOutlet.text = sender.titleLabel?.text
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
    }
    @IBAction func Button2(_ sender: UIButton) {
        SliderFMOutlet.value = Float(sender.titleLabel!.text!)!
        FMlevelOutlet.text = sender.titleLabel?.text
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
    }
    @IBAction func Button3(_ sender: UIButton) {
        SliderFMOutlet.value = Float(sender.titleLabel!.text!)!
        FMlevelOutlet.text = sender.titleLabel?.text
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
    }
    @IBAction func Button4(_ sender: UIButton) {
        SliderFMOutlet.value = Float(sender.titleLabel!.text!)!
        FMlevelOutlet.text = sender.titleLabel?.text
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
    }
    @IBAction func Button5(_ sender: UIButton) {
        SliderFMOutlet.value = Float(sender.titleLabel!.text!)!
        FMlevelOutlet.text = sender.titleLabel?.text
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
    }
    @IBAction func Button6(_ sender: UIButton) {
        SliderFMOutlet.value = Float(sender.titleLabel!.text!)!
        FMlevelOutlet.text = sender.titleLabel?.text
        SliderFMOutlet.value = Float(FMlevelOutlet.text!)!
    }
    
    @IBAction func LongRecognizerAction1(_ sender: UILongPressGestureRecognizer) {
        Button1.setTitle(FMlevelOutlet.text!, for: .normal)
        AudioServicesPlaySystemSound(1520)
    }
    @IBAction func LongRecognizerAction2(_ sender: UILongPressGestureRecognizer) {
        Button2.setTitle(FMlevelOutlet.text!, for: .normal)
        AudioServicesPlaySystemSound(1520)
    }
    @IBAction func LongRecognizerAction3(_ sender: UILongPressGestureRecognizer) {
        Button3.setTitle(FMlevelOutlet.text!, for: .normal)
        AudioServicesPlaySystemSound(1520)
    }
    @IBAction func LongRecognizerAction4(_ sender: UILongPressGestureRecognizer) {
        Button4.setTitle(FMlevelOutlet.text!, for: .normal)
        AudioServicesPlaySystemSound(1520)
    }
    @IBAction func LongRecognizerAction5(_ sender: UILongPressGestureRecognizer) {
        Button5.setTitle(FMlevelOutlet.text!, for: .normal)
        AudioServicesPlaySystemSound(1520)
    }
    @IBAction func LongRecognizerAction6(_ sender: UILongPressGestureRecognizer) {
        Button6.setTitle(FMlevelOutlet.text!, for: .normal)
        AudioServicesPlaySystemSound(1520)
    }
    
    
    @IBAction func FMLeft(_ sender: UIButton) {
        if (Float(FMlevelOutlet.text!)! - 0.1) > 87.9 {
            FMlevelOutlet.text = String(format: "%.1f", (Float(FMlevelOutlet.text!)! - 0.1))
            SliderFMOutlet.value = Float(FMlevelOutlet.text!)! }
    }
    @IBAction func FMRight(_ sender: UIButton) {
        if (Float(FMlevelOutlet.text!)! + 0.1) < 108.1 {
            FMlevelOutlet.text = String(format: "%.1f", (Float(FMlevelOutlet.text!)! + 0.1))
            SliderFMOutlet.value = Float(FMlevelOutlet.text!)! }
    }
    @IBAction func FMSliderAction(_ sender: UISlider) {
        FMlevelOutlet.text = String(format: "%.1f", (sender.value))
    }
    
    @IBAction func Swipe(_ sender: UISwipeGestureRecognizer) {
        sender.direction = .left
        performSegue(withIdentifier: "JumpToEqa", sender: nil)
    }
    
    @IBAction func SliderVolumeAction(_ sender: UISlider) {
        if sender .value == 0 {
            SoundLevelIndOutlet.setImage(UIImage(named: "MuteRed"), for: .normal)
            RadioButtonOutlet.setImage(UIImage(named: "PhoneButton"), for: .normal)
        } else if sender.value > 0 {
            SoundLevelIndOutlet.setImage(UIImage(named: "Sound"), for: .normal)
            RadioButtonOutlet.setImage(UIImage(named: "RadioButtom"), for: .normal)
        }
    }
}

extension ViewController {
    func muteSound () {
        if mute == false {
        mute = true
        SoundLevelIndOutlet.setImage(UIImage(named: "MuteRed"), for: .normal)
            RadioButtonOutlet.setImage(UIImage(named: "PhoneButton"), for: .normal) }
        else if mute == true {
            SoundLevelIndOutlet.setImage(UIImage(named: "Sound"), for: .normal)
            RadioButtonOutlet.setImage(UIImage(named: "RadioButtom"), for: .normal)
            mute = false
        }
    }
}
