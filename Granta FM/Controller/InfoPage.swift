//
//  InfoPage.swift
//  Granta FM
//
//  Created by Юрий Ершов on 02.08.2019.
//  Copyright © 2019 ITELMA. All rights reserved.
//

import UIKit

class InfoPage: UIViewController {

    @IBOutlet var BackRecognize: UISwipeGestureRecognizer!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    
    @IBAction func RecognBackToAlarm(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "JumpBackToAlarm", sender: nil)
    }
    
    @IBAction func GesturTape(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    @IBAction func SetNumberLice(_ sender: UIButton) {
        textField.resignFirstResponder()
    }
}

