//
//  LockViewController.swift
//  Granta FM
//
//  Created by Юрий Ершов on 01.08.2019.
//  Copyright © 2019 ITELMA. All rights reserved.
//

import UIKit
import AudioToolbox

class LockViewController: UIViewController {

    @IBOutlet weak var LockButton: UIButton!
    @IBOutlet weak var UnLockButton: UIButton!
    
    @IBOutlet var BackRecognize: UISwipeGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func RecognToLock(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "JumpToEqBack", sender: nil)
    }
    @IBAction func RecognToTemp(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "JumpToTemp", sender: nil)
    }
    
    
    
    @IBAction func LockRecognize(_ sender: UILongPressGestureRecognizer) {
        AudioServicesPlaySystemSound(1520)
        buttonAnimation(sender: LockButton)
        LockButton.setImage(UIImage(named: "LockButtonRed"), for: .normal)
        UnLockButton.setImage(UIImage(named: "UnlockBotton"), for: .normal)
    }
    @IBAction func UnLockRecognize(_ sender: UILongPressGestureRecognizer) {
        AudioServicesPlaySystemSound(1520)
        buttonAnimation(sender: UnLockButton)
        LockButton.setImage(UIImage(named: "LockButton"), for: .normal)
        UnLockButton.setImage(UIImage(named: "UnlockBottonRed"), for: .normal)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LockViewController {

    func buttonAnimation (sender: UIButton) {
        sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)

        UIView.animate(withDuration: 1.0,
        delay: 0,
        usingSpringWithDamping: CGFloat(0.20),
        initialSpringVelocity: CGFloat(6.0),
        options: UIView.AnimationOptions.allowUserInteraction,
        animations: {
        sender.transform = CGAffineTransform.identity
        },
        completion: { Void in()  }
        )
            }
}
