//
//  EqualizerViewController.swift
//  Granta FM
//
//  Created by Юрий Ершов on 31.07.2019.
//  Copyright © 2019 ITELMA. All rights reserved.
//

import UIKit

class EqualizerViewController: UIViewController {
    
    var soundLevel: soundEQ?
    
    var soundLevelsliderNC = UISlider()
    var soundLevelsliderSC = UISlider()
    var soundLevelsliderVC = UISlider()
    
    @IBOutlet weak var ViewRight: UIView!
    @IBOutlet weak var ViewMid: UIView!
    @IBOutlet weak var ViewLeft: UIView!
    @IBOutlet var RecogniserHome: UISwipeGestureRecognizer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        soundLevel = soundEQ(NC: 50, SC: 50, VC: 50)
        
        castomSlider(slider: soundLevelsliderNC, view: ViewLeft, imageName: "SliderVolThumb2", BaseValue: soundLevel!.NC)
        castomSlider(slider: soundLevelsliderSC, view: ViewMid, imageName: "SliderVolThumb2", BaseValue: soundLevel!.NC)
        castomSlider(slider: soundLevelsliderVC, view: ViewRight, imageName: "SliderVolThumb2", BaseValue: soundLevel!.NC)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func SwipeHome(_ sender: UISwipeGestureRecognizer) {
          performSegue(withIdentifier: "JumpToFirst", sender: nil)
    }
    @IBAction func RecognLockPage(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "JumpToLock", sender: nil)
    }
    
    
}
extension EqualizerViewController {

    func castomSlider(slider: UISlider, view: UIView, imageName: String, BaseValue: Int) -> () {
    let h = view.bounds.height
    let w = view.bounds.width
    slider.bounds.size.width = h
    slider.center = CGPoint(x: w/2, y: h/2)
    slider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
    view.addSubview(slider)
    slider.backgroundColor = .clear
    slider.minimumTrackTintColor = .clear
    slider.maximumTrackTintColor = .clear
    slider.minimumValue = 0
    slider.maximumValue = 100
        if BaseValue > 0 && BaseValue < 101 {
            slider.value = Float(BaseValue)
        }
    slider.isContinuous = true; // false makes it call only once you let go
    slider.addTarget(self, action: #selector (valueChanged(_:)), for: .valueChanged)
    guard let image = UIImage(named: imageName) else {print("Image name incorrect"); return}
    slider.setThumbImage(image, for: .normal)
    slider.setThumbImage(image, for: .selected)
    slider.setThumbImage(image, for: .highlighted)
    }
    @objc func valueChanged (_ sender: UISlider) {
                
    }

}
